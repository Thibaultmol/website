The Panoramax service operated by IGN is made of https://panoramax.ign.fr website and the API https://panoramax.ign.fr/api.

# Legal notice

The editor of https://panoramax.ign.fr website is the National Institute of Geographic and Forest Information (IGN), a public administrative institution under the joint supervision of the ministries responsible for ecology and forestry.

Adress: 73 avenue de Paris – 94165 SAINT-MANDÉ Cedex, France

Phone: +33 1 43 98 80 00

The publication director of this website is Sébastien Soriano, general director of IGN. You can contact him at panoramax@panoramax.fr.

The host is Scalingo.

# Licence

The license for publishing the photos is etalab-2.0 (french open data licence).

The source code of the software of the service is free/libre.
