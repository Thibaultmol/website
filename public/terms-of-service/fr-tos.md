Le service Panoramax opéré par l’IGN est constitué [du site internet](https://panoramax.ign.fr) et de [l’API](https://panoramax.ign.fr/api).

# Mentions légales

L’éditeur du site https://panoramax.ign.fr est l’Institut national de l’information géographique et forestière (IGN), Établissement public administratif sous la double tutelle des ministères respectivement chargés de l’écologie et de la forêt.

Adresse : 73 avenue de Paris – 94165 SAINT-MANDÉ Cedex

Téléphone : 01 43 98 80 00

Le directeur de publication de ce site internet est Sébastien Soriano, directeur général de l'IGN. Vous pouvez le contacter à l’adresse panoramax@panoramax.fr.

L'hébergeur est Scalingo.

# Licence

La licence de publication des photos est etalab-2.0

Le code source du logiciel du service est du logiciel libre.