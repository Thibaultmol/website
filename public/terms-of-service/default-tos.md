# Default legal terms

The Panoramax service operated by [INSTANCE HOSTER ORGANISATION NAME] is made of [https://panoramax.??] website and the API [https://panoramax.??/api].

# Legal notice

The editor of [https://panoramax.??] website is [INSTANCE HOSTER ORGANISATION NAME].

The publication director of this website is [INSTANCE HOSTER FIRST AND LAST NAME].

You can contact them at [EMAIL ADRESS].

The host is [NAME OF THE HOST].

# Licence

The license for publishing the photos is [OPEN DATA LICENCE NAME AND URL].

The source code of the software of the service is free/libre.
