describe('In the home page', () => {
  it('click on the link in the footer to go to Panoramax.fr', () => {
    cy.visit('/')
    cy.fixture('home').then((homeData) => {
      cy.contains(homeData.textLinkPanoramax).click()
    })
  })
  it('click on the link in the footer to go to Gitlab', () => {
    cy.visit('/')
    cy.fixture('home').then((homeData) => {
      cy.contains(homeData.textLinkGitlab).click()
    })
  })
})

export {}
