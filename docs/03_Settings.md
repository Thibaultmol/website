# Settings

Many things can be customized in your GeoVisio Website.

## 1 - Basic settings

Low-level settings can be changed through the `.env` file. An example is given in `env.example` file.

Available parameters are:

- `VITE_API_URL`: the URL to the GeoVisio API (example: `https://panoramax.ign.fr/`)
- `VITE_INSTANCE_NAME`: the name of the instance (example: `IGN`)
- `VITE_TILES`: the URL of your tiles : default tiles are the OpenStreetMap Tiles (example: `https://data.geopf.fr/annexes/ressources/vectorTiles/styles/PLAN.IGN/standard.json`)
- `VITE_MAX_ZOOM`: the max zoom to use on the map (defaults to 24).
- `VITE_ZOOM`: the zoom to use at the initialization of the map (defaults to 0).
- `VITE_CENTER`: the center position to use at the initialization of the map (defaults to 0).
- `VITE_RASTER_TILE`: the raster tile. Example : `https://maplibre.org/maplibre-style-spec/sources/#raster`.
- `VITE_MAP_ATTRIBUTION`: the attribution inside the viewer (example: © IGN).
- `VITE_TITLE`: the title of the <title> tag : (example: `Panoramax IGN : photo-cartographier les territoires`).
- `VITE_META_DESCRIPTION`: the description of the meta tags. Example : `L'instance Panoramax IGN permet la publication de photo de terrain pour cartographier le territoire. Panoramax favorise la réutilisation des photos pour de nombreux cas d'usages.`.
- `VITE_META_TITLE`: the title of the meta tags. Example : `Panoramax IGN : photo-cartographier les territoires`.
- `VITE_LEGAL_TERMS_ACTIVE`: to enable the CGU pages : `true`.
- Settings for the work environment:
  - `VITE_ENV`: `dev`

More settings are available [in official Vite documentation](https://vitejs.dev/guide/env-and-mode.html#env-files)

Note that you can also change the _Vite_ server configuration in the `vite.config.ts` file. See [Vite Configuration Reference](https://vitejs.dev/config/) if you need.

## 2 - Wording customization

GeoVisio website can be customized to have wording reflecting your brand, licence and other elements.

All the wordings of the website are on this [locale file](https://gitlab.com/panoramax/server/website/-/blob/develop/src/locales/en.json). In there, you might want to change:

- The website title (properties `title` and `meta.title`)
- The description (property `meta.description`)
- Links to help pages:
  - `upload.description`
  - `upload.footer_description_terminal`

### 2.1 - Lang customization

There is already files to have custom langage li French and English [here](https://gitlab.com/panoramax/server/website/-/blob/develop/src/locales)
You can edit these files if you want change the wordings or use [the collaborative site Weblate](https://weblate.panoramax.xyz/)
If you want to add a new translation file, you can add it with the structure name `{locale}.json` and put it inside the folder.
After that you need to edit the `index.ts` and add your new locale [here](https://gitlab.com/panoramax/server/website/-/tree/develop/src/config/locales.ts)

### 3.1 - Terms of service customization

If you want to activate the terms of service management, you have to define the `VITE_LEGAL_TERMS_ACTIVE` en var to `true`
Terms of service are defined inside [this folder](https://gitlab.com/panoramax/server/website/-/tree/develop/src/config/terms-of-service) and managed by markdowns files (`.md`).
If `VITE_LEGAL_TERMS_ACTIVE` is `true` and if you have a markdown file with the structure name `{locale}-tos.md` (example: `en-tos.md` ), the file will be used to display the terms of service page.
If `VITE_LEGAL_TERMS_ACTIVE` is `true` but you have only the `default-tos.md`, the default terms of service wordings will be displayed.
You can add any file inside [the folder](https://gitlab.com/panoramax/server/website/-/tree/develop/src/config/terms-of-service) if you want to add terms of service or you can directly modify the existing files.  
If you want to add a new locale, don't forget to edit the `index.ts` and add your new locale [here](https://gitlab.com/panoramax/server/website/-/tree/develop/src/config/locales.ts)

## 3 - Visuals

The following images can be changed to make the website more personal:

- Logo: [`src/assets/images/logo.jpeg`](https://gitlab.com/panoramax/server/website/-/blob/develop/src/assets/images/logo.jpeg)
- Favicon: [`static/favicon.ico`](https://gitlab.com/panoramax/server/website/-/blob/develop/static/favicon.ico)
