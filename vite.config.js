import { fileURLToPath, URL } from 'node:url'
import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import eslintPlugin from 'vite-plugin-eslint'
import { createHtmlPlugin } from 'vite-plugin-html'
import Markdown from 'vite-plugin-vue-markdown'

// https://vitejs.dev/config/
export default ({ mode }) => {
  process.env = { ...process.env, ...loadEnv(mode, process.cwd()) }
  return defineConfig({
    server: {
      host: true,
      port: 5173,
      strictPort: true
    },
    base: '/',
    plugins: [
      vue({
        include: [/\.vue$/, /\.md$/]
      }),
      Markdown(),
      eslintPlugin(),
      createHtmlPlugin({
        minify: true,
        /**
         * Data that needs to be injected into the index.html ejs template
         */
        inject: {
          data: {
            instanceName: process.env.VITE_INSTANCE_NAME,
            frontUrl: process.env.VITE_FRONT_URL
          }
        }
      })
    ],
    publicDir: 'public',
    css: {
      preprocessorOptions: {
        scss: {
          additionalData:
            '@import "@/assets/font-size.scss"; @import "@/assets/rem-calc.scss"; @import "@/assets/components/index.scss";'
        }
      }
    },
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url)),
        'vue-i18n': 'vue-i18n/dist/vue-i18n.cjs.js'
      }
    },
    define: {
      // Define the feature flag for better tree-shaking
      __VUE_PROD_HYDRATION_MISMATCH_DETAILS__: false
    },
    build: {
      target: 'ES2022',
      minify: 'terser',
      terserOptions: {
        mangle: false,
        keep_classnames: true,
        keep_fnames: true
      }
    }
  })
}
