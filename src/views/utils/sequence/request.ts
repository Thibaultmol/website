import axios from 'axios'
import type {
  ResponseUserPhotoInterface,
  ResponseUserPhotoLinksInterface,
  ResponseUserSequenceInterface,
  UserSequenceInterface
} from '@/views/interfaces/MySequenceView'

function deleteACollection(collectionId: string | string[]): Promise<unknown> {
  return axios.delete(`api/collections/${collectionId}`)
}

function deleteAnUploadSet(uploadSetId: string | string[]): Promise<unknown> {
  return axios.delete(`api/upload_sets/${uploadSetId}`)
}

function patchACollection(
  collectionId: string | string[],
  fieldObject: object
): Promise<{ data: ResponseUserSequenceInterface }> {
  return axios.patch(`api/collections/${collectionId}`, fieldObject)
}

function deleteACollectionItem(
  collectionId: string | string[],
  itemId: string
): Promise<{ data: UserSequenceInterface }> {
  return axios.delete(`api/collections/${collectionId}/items/${itemId}`)
}

function patchACollectionItem(
  isVisible: string,
  collectionId: string | string[],
  itemId: string
): Promise<unknown> {
  return axios.patch(`api/collections/${collectionId}/items/${itemId}`, {
    visible: isVisible
  })
}

async function fetchCollectionItems(
  collectionId: string | string[],
  limit: string,
  fullUrl?: string
): Promise<{
  data: {
    features: [ResponseUserPhotoInterface]
    links: [ResponseUserPhotoLinksInterface]
  }
}> {
  if (fullUrl) return await axios.get(fullUrl)
  return await axios.get(`api/collections/${collectionId}/items${limit}`)
}

async function fetchCollectionItemsWithFullUrl(fullUrl: string): Promise<{
  data: {
    features: [ResponseUserPhotoInterface]
    links: [ResponseUserPhotoLinksInterface]
  }
}> {
  return await axios.get(fullUrl)
}

async function fetchCollection(collectionId: string | string[]): Promise<{
  data: ResponseUserSequenceInterface
}> {
  return await axios.get(`api/collections/${collectionId}`)
}

export {
  deleteACollectionItem,
  patchACollectionItem,
  fetchCollectionItems,
  fetchCollection,
  deleteACollection,
  deleteAnUploadSet,
  patchACollection,
  fetchCollectionItemsWithFullUrl
}
