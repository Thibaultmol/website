import axios from 'axios'
import type { UploadSetCreatedInterface } from '../../interfaces/UploadPicturesView'

interface PictureCreatedInterface {
  data: {
    assets: object
    bbox: number[]
    collection: string
    geometry: object
    id: string
    links: object[]
    properties: object
    providers: object[]
    stac_extensions: string[]
    stac_version: string
    type: string
  }
}

function createUploadSets(
  title: string,
  estimated_nb_files: number
): Promise<{ data: UploadSetCreatedInterface }> {
  return axios.post('api/upload_sets', { title, estimated_nb_files })
}

async function createAPictureToAnUploadSet(
  id: string,
  body: FormData
): Promise<PictureCreatedInterface> {
  const { data } = await axios.post(`api/upload_sets/${id}/files`, body)
  return data
}

function fetchUploadSet(uploadSetId: string): Promise<{
  data: UploadSetCreatedInterface
}> {
  return axios.get(`api/upload_sets/${uploadSetId}`)
}

function fetchUploadSets(): Promise<{
  data: { upload_sets: UploadSetCreatedInterface[] }
}> {
  return axios.get(
    `api/users/me/upload_sets?filter=${encodeURIComponent('completed=FALSE')}`
  )
}

export {
  createUploadSets,
  createAPictureToAnUploadSet,
  fetchUploadSet,
  fetchUploadSets
}
