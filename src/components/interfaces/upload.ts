export interface HTMLInputChangeEvent extends Event {
  target: HTMLInputElement & EventTarget
}
export interface FileSystemEntry {
  isFile: boolean
  isDirectory: boolean
  name: string
  fullPath: string
  filesystem: any
  getParent: (
    successCallback: (entry: FileSystemEntry) => void,
    errorCallback?: (error: DOMException) => void
  ) => void
}

export interface FileSystemFileEntry extends FileSystemEntry {
  file: (
    successCallback: (file: File) => void,
    errorCallback?: (error: DOMException) => void
  ) => void
}

export interface FileSystemDirectoryEntry extends FileSystemEntry {
  createReader: () => FileSystemDirectoryReader
}

export interface FileSystemDirectoryReader {
  readEntries: (
    successCallback: (entries: FileSystemEntry[]) => void,
    errorCallback?: (error: DOMException) => void
  ) => void
}
