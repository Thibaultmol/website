import { createI18n } from 'vue-i18n'
import { useCookies } from 'vue3-cookies'
import { importLocales } from '../utils'
const { cookies } = useCookies()
let messages

await importLocales().then((locales) => {
  messages = locales
})
const locale = cookies.get('lang') ? cookies.get('lang') : autoDetectLocale()

const i18n = createI18n({
  locale,
  fallbackLocale: 'fr',
  warnHtmlMessage: false,
  globalInjection: true,
  legacy: false,
  messages
})
export default i18n

function autoDetectLocale() {
  for (const navigatorLang of window.navigator.languages) {
    let language = navigatorLang
    switch (language) {
      case 'zh-TW':
      case 'zh-HK':
      case 'zh-MO':
        language = 'zh_Hant'
        break
      case 'zh-CN':
      case 'zh-SG':
        language = 'zh_Hans'
        break
      default:
        if (language.length > 2) {
          language = navigatorLang.substring(0, 2)
        }
        break
    }
    const pair = Object.entries(messages).find((pair) => pair[0] === language)
    if (pair) {
      return pair[0]
    }
  }
  return 'en'
}
