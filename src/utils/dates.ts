import moment from 'moment'
import 'moment/dist/locale/fr'

function formatDate(date: Date | null | string, formatType: string): string {
  const formatDate = moment(date)
  return formatDate.locale(window.navigator.language).format(formatType)
}

function durationCalc(duration1: Date, duration2: Date, type: string): number {
  const duration = moment.duration(moment(duration1).diff(moment(duration2)))
  if (type == 'hours') return duration.hours()
  if (type == 'minutes') return duration.minutes()
  return duration.seconds()
}

export { formatDate, durationCalc }
