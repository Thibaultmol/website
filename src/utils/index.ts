import { ACTIVE_LOCALES } from '@/locales'

function createUrlLink(picId: string): string {
  return encodeURIComponent(`${window.location.origin}/#focus=pic&pic=${picId}`)
}

function manageSlashUrl(): string {
  let apiUrl = getEnv('VITE_API_URL')
  if (apiUrl.charAt(apiUrl.length - 1) !== '/') apiUrl += '/'
  return apiUrl
}

function getEnv(value: string | undefined) {
  if (value) return import.meta.env[value]
  return null
}

async function importLocales() {
  const imports = ACTIVE_LOCALES.map((locale: string) =>
    import(`../locales/${locale}.json`).then((module) => ({
      [locale]: module.default
    }))
  )

  const locales = await Promise.all(imports)
  return Object.assign({}, ...locales)
}

export { createUrlLink, manageSlashUrl, getEnv, importLocales }
