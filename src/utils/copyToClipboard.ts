export async function updateClipboard(newClip: string): Promise<boolean> {
  return await navigator.clipboard.writeText(newClip).then(
    () => true,
    () => false
  )
}
