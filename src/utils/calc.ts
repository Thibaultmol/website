function modulo180(value: number, roadDegrees: number): number {
  let moduloAngle = (value - roadDegrees) % 360
  while (moduloAngle > 180 || moduloAngle < -180) {
    if (moduloAngle < -180) moduloAngle += 360
    if (moduloAngle > 180) moduloAngle -= 360
  }
  return Math.round(moduloAngle)
}

export { modulo180 }
