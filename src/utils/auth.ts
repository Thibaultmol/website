import pako from 'pako'
import { useCookies } from 'vue3-cookies'
import { manageSlashUrl } from '@/utils'
const { cookies } = useCookies()

function getAuthRoute(authRoute: string, nextRoute: string): string {
  const next = `${location.protocol}//${location.host}${nextRoute}`
  return `${manageSlashUrl()}api/${authRoute}?next_url=${encodeURIComponent(
    `${next}`
  )}`
}

// This function to decode the flask cookie and have the user information like username
function decodingFlaskCookie(cookie: string): string {
  const compressed = cookie.substring(0, 1) === '.'
  const dataPart = compressed ? 1 : 0
  const cookieFormatted = cookie
    .split('.')
    [dataPart].replace(/_/g, '/')
    .replace(/-/g, '+')
  const binaryString = atob(cookieFormatted)
  const bytes = new Uint8Array(binaryString.length)
  for (let i = 0; i < binaryString.length; i++) {
    bytes[i] = binaryString.charCodeAt(i)
  }
  if (compressed) return pako.inflate(bytes.buffer, { to: 'string' })
  return String.fromCharCode(...bytes)
}
function sessionCookieDecoded(): {
  account: { name: string; id: string; oauth_id: string }
} | null {
  if (cookies.get('session')) {
    return JSON.parse(decodingFlaskCookie(cookies.get('session')))
  }
  return null
}

function formatUserName(): string {
  const cookie = sessionCookieDecoded()
  if (cookie && cookie.account) {
    return cookie.account.name
      .match(/\b(\w)/g)!
      .join('')
      .toUpperCase()
  }
  return ''
}

export {
  getAuthRoute,
  sessionCookieDecoded,
  decodingFlaskCookie,
  formatUserName
}
