import axios from 'axios'
import { ACTIVE_LOCALES } from '@/locales'

function formatLocalesPath(): Record<string, string> {
  const imports = ACTIVE_LOCALES.map((locale: string) => {
    return {
      [locale]: `/terms-of-service/${locale}-tos.md`
    }
  })
  return Object.assign({}, ...imports)
}

export async function loadMarkdown(locale: string) {
  const checkFileExists = async (filePath: string) => {
    try {
      return await axios.get(filePath)
    } catch (error) {
      return false
    }
  }
  const defaultCguPath = '/terms-of-service/default-tos.md'

  const paths = formatLocalesPath() as Record<string, string>

  let files = {} as Record<string, string>
  for (const [key, value] of Object.entries(paths)) {
    files = { ...files, [key]: new URL(value, import.meta.url).toString() }
  }
  const filePath = paths[locale]
  const uri = (path: string) => `${window.location.origin}${path}`
  try {
    const exists = await checkFileExists(files[locale])
    if (exists && !exists.data.includes('<!doctype html>'))
      return await axios.get(uri(filePath))
    else return await await axios.get(uri(defaultCguPath))
  } catch (e) {
    return await axios.get(uri(defaultCguPath))
  }
}
