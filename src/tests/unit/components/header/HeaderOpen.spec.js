import { vi, it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import HeaderOpen from '../../../../components/header/HeaderOpen.vue'
import Link from '../../../../components/Link.vue'
import i18n from '../../config'
vi.mock('vue-router', () => ({
  useRoute: () => ({
    path: '/mock-path'
  })
}))
import.meta.env.VITE_API_URL = 'api-url/'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(HeaderOpen, {
        global: {
          plugins: [i18n]
        }
      })
      expect(wrapper.vm.menuIsClosed).toBe(true)
      expect(wrapper.vm.userName).toBe('')
      expect(wrapper.vm.userProfileUrlLength).toBe(0)
    })
    describe('When all the props are filled', () => {
      it('should render the entries Link', () => {
        const wrapper = shallowMount(HeaderOpen, {
          global: {
            plugins: [i18n],
            stubs: { Link: false }
          },
          props: {
            menuIsClosed: false,
            userName: 'name',
            userProfileUrlLength: 5
          }
        })
        const routes = ['my-sequences', 'my-information', 'my-settings']

        const linkComponents = wrapper.findAllComponents(Link)
        const routeNames = linkComponents.map((link) => link.props().route.name)
        routes.forEach((route) => {
          expect(routeNames).toContain(route)
        })
        expect(wrapper.html()).contains('Mes photos')
        expect(wrapper.html()).contains('Mes informations')
        expect(wrapper.html()).contains('Mes paramètres')
        expect(wrapper.html()).contains('Déconnexion')
        expect(wrapper.html()).contains('href="api-url/api/auth/logout')
      })
    })
  })
})
