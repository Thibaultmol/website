import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import MyInformationView from '../../../views/MyInformationView.vue'

describe('Template', () => {
  it('should render the view with the iframe', async () => {
    import.meta.env.VITE_API_URL = 'api-url/'
    const wrapper = shallowMount(MyInformationView)

    expect(wrapper.html()).contains('<iframe')
    expect(wrapper.html()).contains('geovisio/account')
  })
})
